# Assignment #4: Decision Trees 

This markdown file describes the Jupyter Notebook containing code for Assignment #4. The notebook covers the implementation of Decision Trees, Boosting, and Bagging algorithms for regression tasks using cross-validation.

## Loading the Dataset
- Imports necessary libraries.
- Fetches the dataset from the UCI Machine Learning Repository.
- Splits the dataset into features (X) and target variables (y).
- Splits the data into training and testing sets.

## Decision Tree
- Selects the best method for choosing the number of max features using 5-fold cross-validation.
- Performs cost complexity pruning to find the optimal alpha value with cross-validation.
- Plots the best decision tree and visualizes feature importance.
- Evaluates the performance of the decision tree on the test data.

## Random Forest
- Tests different values for max features using 5-fold cross-validation.
- Conducts a grid search to find the optimal number of estimators with cross-validation.
- Fits the final Random Forest model and evaluates its performance on the test data.

## Conclusion
- Summarizes the findings from Decision Tree and Random Forest regressions, highlighting the use of cross-validation for model evaluation.
- Random Forest has outperformed decision tree regressor in terms of test RMSE.
